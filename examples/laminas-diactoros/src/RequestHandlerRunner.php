<?php

require '../vendor/autoload.php';


use Laminas\Diactoros\Response;
use Laminas\Diactoros\Response\TextResponse;
use Laminas\Diactoros\ServerRequest;
use Laminas\Diactoros\ServerRequestFactory;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;
use Laminas\HttpHandlerRunner\RequestHandlerRunner;
use Laminas\Stratigility\Middleware\ErrorResponseGenerator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
// use Throwable;


(new RequestHandlerRunner(
    new class
        implements
            RequestHandlerInterface {

        public function handle(ServerRequestInterface $_request): ResponseInterface
        {
            return new TextResponse('Hello world!');
        }
    },
    new SapiEmitter,
    [ServerRequestFactory::class, 'fromGlobals'],
    function (Throwable $e) {
        return (new ErrorResponseGenerator())($e, new ServerRequest(), new Response());
    }
))->run();
