<?php

require '../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$request = Request::createFromGlobals();
$response = new Response(
    $request->getPathInfo(),
    Response::HTTP_OK,
    ['content-type' => 'text/plain']
);
$response->prepare($request);
$response->send();

?>
