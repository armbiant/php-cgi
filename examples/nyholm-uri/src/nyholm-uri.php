<?php

require '../vendor/autoload.php';

use Nyholm\Psr7\Factory\Psr17Factory;
use Nyholm\Psr7Server\ServerRequestCreator;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Server\RequestHandlerInterface;


$psr17Factory = new Psr17Factory();

echo (new class($psr17Factory, $psr17Factory)
        implements
            RequestHandlerInterface {
        public function __construct(
            private readonly ResponseFactoryInterface $ResponseFactory,
            private readonly StreamFactoryInterface $StreamFactory
        ){}

        public function handle(ServerRequestInterface $request): ResponseInterface
        {
            return $this->ResponseFactory->createResponse()->withBody($this->StreamFactory->createStream($request->getUri()->getPath()));
        }
    }
)->handle((new ServerRequestCreator(
        $psr17Factory, // ServerRequestFactory
        $psr17Factory, // UriFactory
        $psr17Factory, // UploadedFileFactory
        $psr17Factory  // StreamFactory
    ))->fromGlobals()
)->getBody();

?>
