<?php
declare(strict_types=1);

require '../vendor/autoload.php';


use Laminas\Diactoros\Response;
use Laminas\Diactoros\ResponseFactory;
use Laminas\Diactoros\ServerRequest;
use Laminas\Diactoros\ServerRequestFactory;
use Laminas\Diactoros\StreamFactory;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;
use Laminas\HttpHandlerRunner\RequestHandlerRunner;
use Laminas\Stratigility\Middleware\ErrorResponseGenerator;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Server\RequestHandlerInterface;


(new RequestHandlerRunner(
    new class(new ResponseFactory, new StreamFactory)
        implements
            RequestHandlerInterface {
        public function __construct(
            private readonly ResponseFactoryInterface $ResponseFactory,
            private readonly StreamFactoryInterface $StreamFactory
        ){}

        public function handle(ServerRequestInterface $request): ResponseInterface
        {
            return $this->ResponseFactory->createResponse()->withBody($this->StreamFactory->createStream('Hello world!'));
        }
    },
    new SapiEmitter,
    [ServerRequestFactory::class, 'fromGlobals'],
    function (Throwable $e) {
        return (new ErrorResponseGenerator())($e, new ServerRequest(), new Response());
    }
))->run();
